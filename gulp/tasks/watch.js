module.exports = function() {
	$.gulp.task("watch", async function() {
		return new Promise((res, rej) => {
			$.gp.watch("./app/**/*.html", $.gulp.series("html"));
			$.gp.watch("./app/js/**/*.js", $.gulp.series("scripts"));
			$.gp.watch("./app/scss/common/**/*.scss", $.gulp.parallel("styles", "criticalCss"));
			$.gp.watch("./app/scss/critical/**/*.scss", $.gulp.series("criticalCss", "styles"));
			$.gp.watch("./app/scss/vendor/**/*.scss", $.gulp.series("vendor"));
			res();
		});
	});
};